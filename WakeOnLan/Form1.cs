﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace WakeOnLan
{
    public partial class Form1 : Form
    {
        private const string FilePath = "Config.xml";

        public Form1()
        {
            InitializeComponent();
        }

        private static void Serialize(Config config)
        {
            var serializer = new XmlSerializer(typeof(Config));
            using (var writer = new StreamWriter(FilePath))
            {
                try
                {
                    serializer.Serialize(writer, config);
                }
                catch (Exception exception)
                {

                    MessageBox.Show(exception.Message);
                }

            }
        }

        private static Config Deserialize()
        {
            var serializer = new XmlSerializer(typeof(Config));
            using (var reader = new StreamReader(FilePath))
            {
                try
                {
                    return serializer.Deserialize(reader) as Config;
                }
                catch (Exception exception)
                {

                    MessageBox.Show(exception.Message);
                    return null;
                }

            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (!File.Exists(FilePath)) return;
            var config = Deserialize();
            textBoxHost.Text = config.Host;
            textBoxMac.Text = config.MacAdres;
            numericUpDownPort.Value = config.Port;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            var config = new Config()
            {
                Host = textBoxHost.Text,
                MacAdres = textBoxMac.Text,
                Port = (int)numericUpDownPort.Value
            };
            Serialize(config);
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            var host = textBoxHost.Text;
            var mac = textBoxMac.Text.Replace(":", "").Replace("-", "");
            var port = (int)numericUpDownPort.Value;
            var wakeOnLan = new WakeOnLan();
            try
            {
                MessageBox.Show(string.Format("Magic packet отправлен на {0}", wakeOnLan.WakeFunction(host, mac, port)));
            }
            catch (Exception exception)
            {

                MessageBox.Show(exception.Message);
            }



        }
    }
}
