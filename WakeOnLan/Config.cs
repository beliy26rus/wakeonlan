﻿namespace WakeOnLan
{
    public class Config
    {
        public string Host { get; set; }
        public string MacAdres { get; set; }
        public int Port { get; set; }
    }
}
