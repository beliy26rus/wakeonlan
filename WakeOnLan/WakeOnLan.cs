﻿using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace WakeOnLan
{
    class WakeOnLan : UdpClient
    {

        public string WakeFunction(string host, string macAddress, int port)
        {
            if (Active)
                Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 0);
            var adres = Dns.GetHostByName(host).AddressList.First();
            Connect(adres, port);
            int counter = 0;
            byte[] bytes = new byte[1024];
            for (int y = 0; y < 6; y++)
                bytes[counter++] = 0xFF;
            for (int y = 0; y < 16; y++)
            {
                int i = 0;
                for (int z = 0; z < 6; z++)
                {
                    bytes[counter++] = byte.Parse(macAddress.Substring(i, 2), NumberStyles.HexNumber);
                    i += 2;
                }
            }
            Send(bytes, 1024);
            return adres.ToString();
        }
    }
}
